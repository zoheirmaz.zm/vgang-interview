import requests
from celery import Celery

from config import Celery_BROKER_URL, MAX_RETRY

app = Celery('tasks', broker=Celery_BROKER_URL)


@app.task(bind=True, autoretry_for=(Exception,), max_retries=MAX_RETRY)
def perform_request(self):
    try:
        response = requests.get('https://uat.vgang.io/api/vgang-core/v1/common/sample-failing-request').json()
        if response['message'] == 'Cannot connect to database':
            raise Exception()

        print(response['message'])
    except Exception:
        raise self.retry(countdown=2 ** self.request.retries)
